package com.availity.exam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvailityCodingExamApplication {

    public static void main(String[] args) {
        SpringApplication.run(AvailityCodingExamApplication.class, args);
    }

}
