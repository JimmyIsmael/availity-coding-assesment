package com.availity.exam.controllers;

import com.availity.exam.services.FileReaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CSVController {

    @Autowired
    FileReaderService fileReaderService;

    @RequestMapping("/")
    public String loadHomePage(Model model){
        return "index";
    }

    @RequestMapping("/readFile")
    public String readFile(Model model){
        model.addAttribute("result", fileReaderService.readFile());
        return "results";
    }
}
