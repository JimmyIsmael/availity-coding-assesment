package com.availity.exam.pojo;

public class Enrollee {

    public Enrollee(String userId, String fullName, Integer version, String ensuranceCompany) {
        this.userId = userId;
        this.fullName = fullName;
        this.version = version;
        this.ensuranceCompany = ensuranceCompany;
    }

    private String userId;
    private String fullName;
    private Integer version;
    private String ensuranceCompany;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getEnsuranceCompany() {
        return ensuranceCompany;
    }

    public void setEnsuranceCompany(String ensuranceCompany) {
        this.ensuranceCompany = ensuranceCompany;
    }

    @Override
    public String toString() {
        return "Enrollee{" +
                "userId='" + userId + '\'' +
                ", fullName='" + fullName + '\'' +
                ", version=" + version +
                ", ensuranceCompany='" + ensuranceCompany + '\'' +
                '}';
    }
}
