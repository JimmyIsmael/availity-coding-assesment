package com.availity.exam.services;

import com.availity.exam.pojo.Enrollee;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

@Service
public class FileReaderService {
    List<Enrollee> enrollees = new ArrayList<>();
    public String readFile(){
        String result="";
        BufferedReader fileReader = null;

        try {

            String line;
            fileReader = new BufferedReader(new FileReader("inbound/enrollees.csv"));

            // Read CSV header
            fileReader.readLine();

            // Read inbound data line by line
            while ((line = fileReader.readLine()) != null) {
                String[] data = line.split(",");
                if (data.length > 0) {
                    Enrollee newRecord = new Enrollee(data[0],data[1]+' '+data[2],Integer.parseInt(data[3]),data[4]);

                    // insertRecords validate the user id per Insurance Company and just saves the highest version
                    insertRecords(newRecord);
                }
            }

            //Cleaning outbound directory
            Path folder = Paths.get("outbound");
            cleanFolder(folder);

            //Sorting Enrollees list by fullName Ascending
            enrollees.sort(Comparator.comparing(Enrollee::getFullName).reversed());

            //Looping through Enrollee class to create separate files
            for (Enrollee enrollee : enrollees) {
                writeFiles("outbound/"+enrollee.getEnsuranceCompany()+".csv",enrollee);
            }

            result="File read successfully.";
        } catch (Exception e) {
            result="Error reading enrolless file.";
            e.printStackTrace();
        } finally {
            try {
                fileReader.close();
            } catch (IOException e) {
                result="Error while closing file reader.";
                e.printStackTrace();
            }
        }
        enrollees.clear();
        return result;
    }


    private String writeFiles(String filePath,Enrollee enrolleeToAdd){
        String result="";
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(filePath,true);
            fileWriter.append(enrolleeToAdd.getUserId());
            fileWriter.append(',');
            fileWriter.append(enrolleeToAdd.getFullName());
            fileWriter.append(',');
            fileWriter.append(enrolleeToAdd.getVersion().toString());
            fileWriter.append(',');
            fileWriter.append(enrolleeToAdd.getEnsuranceCompany());
            fileWriter.append('\n');

            result="File written successfully.";
        } catch (Exception e) {
            result="Error writing enrolless file.";
            e.printStackTrace();
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                result="Error writing enrolless file.";
                e.printStackTrace();
            }
        }
        return result;
    }

    private void insertRecords(Enrollee newRecord){
        if (enrollees.isEmpty()){
            enrollees.add(newRecord);
        }else {
            List<Enrollee> toRemove=new ArrayList<>();
            for (Enrollee existingRecord: enrollees){
                if(existingRecord.getUserId().equals(newRecord.getUserId()) &&
                        existingRecord.getEnsuranceCompany().equals(newRecord.getEnsuranceCompany()) &&
                        existingRecord.getVersion()<newRecord.getVersion()){
                    toRemove.add(existingRecord);
                }
            }
            enrollees.removeAll(toRemove);
            enrollees.add(newRecord);
        }
    }

    private void cleanFolder(Path folder) throws IOException {
        Function<Path, Stream<Path>> walk = p -> {
            try { return Files.walk(p);
            } catch (IOException e) {
                return Stream.empty();
            }};

        Consumer<Path> delete = p -> {
            try {
                Files.delete(p);
            } catch (IOException e) {
            }
        };

        Files.list(folder)
                .flatMap(walk)
                .sorted(Comparator.reverseOrder())
                .forEach(delete);
    }
}
