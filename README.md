# Availity Coding Assesment

## This repository contains the three coding exercises required in the Availity Availity Coding Assesment.

1. LISP code validator is done in plain Java.
2. Frontend assessment is done with Angular 5 and Bootstrap.
3. CSV processor is done in Java and Spring Boot using  Thymeleaf as template engine.