package com.availity.validator;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	String codeToValidate="(defun factorial (N)'Compute the factorial of N.'(if (= N 1)1(* N (factorial (- N 1)))))";

    List<Character> openedBrackets=new ArrayList<Character>();
    List<Character> closedBrackets=new ArrayList<Character>();

        // Looping trough the string to validate to find opening and closing brackets
        System.out.print("Code to Validate: \n"+codeToValidate);
        for (int i = 0; i < codeToValidate.length(); i++) {
            if (codeToValidate.charAt(i)=='('){
                openedBrackets.add(codeToValidate.charAt(i));
            }
            if (codeToValidate.charAt(i)==')'){
                closedBrackets.add(codeToValidate.charAt(i));
            }
        }

        System.out.println("\n");
        System.out.println("Opening Brackets: "+ openedBrackets.toString());
        System.out.println("Closing Brackets: "+closedBrackets.toString());

        // Validating that opening and closing brackets are the same zise
        // The code is aether missing an opening or closing bracket or has an extra one.
        if(openedBrackets.size()!=closedBrackets.size()){
            System.out.println("\nError: The code is aether missing an opening or closing bracket or has an extra one.");
        }
        else {
            System.out.println("\nSuccess: Code validated successfully.");
        }
    }
}
